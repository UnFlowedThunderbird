var UnFlowed = {
  onLoad: function() {
    // initialization code
    this.initialized = true;
  },

  callProgram: function(path, args) {
    try {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
    } catch (e) {
      alert("Permission to read file was denied.");
    }

    var file = Components
      .classes["@mozilla.org/file/local;1"]
      .createInstance(Components.interfaces.nsILocalFile);

    file.initWithPath(path);

    var process = Components
      .classes["@mozilla.org/process/util;1"]
      .createInstance(Components.interfaces.nsIProcess);

    process.init(file);
    process.run(true, args, args == null ? 0 : args.length);
  },

  plaintext2html: function (str) {
    return "<pre>"
      + str.replace("&", "&amp;").replace("\"", "&quot;")
      .replace("<", "&lt;").replace(">", "&gt;").replace("\n", "<br>")
      + "</pre>";
  },

  readFile: function (path) {
    try {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
    } catch (e) {
      alert("Permission to read file was denied.");
    }
    var file = Components.classes["@mozilla.org/file/local;1"]
      .createInstance(Components.interfaces.nsILocalFile);
    file.initWithPath(path);
    if (file.exists() == false) {
      alert("File does not exist");
    }
    var stream = Components.classes["@mozilla.org/network/file-input-stream;1"]
      .createInstance(Components.interfaces.nsIFileInputStream);
    stream.init(file, 1, 4, null);
    var scriptable = Components
      .classes["@mozilla.org/scriptableinputstream;1"]
      .createInstance(Components.interfaces.nsIScriptableInputStream);
    scriptable.init(stream);
    var output = scriptable.read(scriptable.available());
    stream.close();
    return output;
  },

  startsWith: function (str, prefix) {
    return str.substring(0, prefix.length) == prefix;
  },

  openComposer: function (str) {
    var params = Components
      .classes["@mozilla.org/messengercompose/composeparams;1"]
      .createInstance(Components.interfaces.nsIMsgComposeParams);
    params.composeFields = Components
      .classes['@mozilla.org/messengercompose/composefields;1']
      .createInstance(Components.interfaces.nsIMsgCompFields);
    params.format = Components.interfaces.nsIMsgCompFormat.HTML;
    params.type = Components.interfaces.nsIMsgCompType.New;
    params.composeFields.bodyIsAsciiOnly = false;
    params.composeFields.forcePlainText = true;
    params.identity = accountManager
      .getFirstIdentityForServer(GetLoadedMsgFolder().server);

    // strip carriage returns
    str = str.replace("\r", "");
    if (this.startsWith(str, "From ")) {
      // we got a header
      var headerEnd = str.indexOf("\n\n");
      if (headerEnd > 0) {
        var begin = 0, end;
        while ((end = str.indexOf("\n", begin)) > 0 && end > begin) {
          var line = str.substring(begin, end);
          begin = end + 1;
          var colon = line.indexOf(":");
	  if (colon < 0)
            continue;
          var key = line.substring(0, colon).toLowerCase();
          while (line.substring(++colon, colon) == " ");
          var value = line.substring(colon);
          if (key == "to")
            params.composeFields.to = value;
          else if (key == "cc")
            params.composeFields.cc = value;
          else if (key == "subject")
            params.composeFields.subject = value;
        }
        str = str.substring(headerEnd + 2);
      }
    }

    /*
     * We need to convert "str" to preformatted HTML to prevent Thunderbird
     * from swallowing our precious whitespace;  Uggaly, uggaly, wittle wabbit!
     * Funny thing is: when not converting to HTML, the composer window shows
     * the whitespace, but saving to the Drafts folder will fsck it up.
     */
    params.composeFields.body = this.plaintext2html(str);

    msgComposeService.OpenComposeWindowWithParams(null, params);
  },

  onMenuItemCommand: function() {
    var nsIFilePicker = Components.interfaces.nsIFilePicker;
    var filePicker = Components.classes["@mozilla.org/filepicker;1"]
      .createInstance(nsIFilePicker);
    filePicker.init(window, "Select one or more patches",
      nsIFilePicker.modeOpenMultiple);
    filePicker.appendFilter("Patch files", "*.patch");
    filePicker.appendFilters(nsIFilePicker.filterAll);
    var res = filePicker.show();
    if (res == nsIFilePicker.returnOK) {
      var files = filePicker.files;
      while (files.hasMoreElements()) {
        var path = files.getNext()
          .QueryInterface(Components.interfaces.nsILocalFile).path;
        this.openComposer(this.readFile(path));
      }
    }
  }
};

window.addEventListener("load", function(e) { UnFlowed.onLoad(e); }, false); 
